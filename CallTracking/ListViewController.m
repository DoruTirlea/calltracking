//
//  ListViewController.m
//  CallTracking
//
//  Created by Tirlea Doru on 7/27/14.
//  Copyright (c) 2014 VoiceMailTel.Inc. All rights reserved.
//

#import "ListViewController.h"
#import "ViewController.h"
#import "ListTableCell.h"

@interface ListViewController ()

@end

@implementation ListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _datePicker = [[UIDatePicker alloc]init];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:nil action:@selector(doneEditingDate)];
    UIBarButtonItem *flexibleSpaceMiddle = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:nil action:@selector(closeEditingDate)];
    [toolbar setItems:[NSArray arrayWithObjects:doneButton, flexibleSpaceMiddle, closeButton, nil]];
    
    _dateToolbar = toolbar;
    
    _dateFrom.inputView = _datePicker;
    _dateFrom.inputAccessoryView = _dateToolbar;
    
    _dateTo.inputView = _datePicker;
    _dateTo.inputAccessoryView = _dateToolbar;
    
    _currentPlaying =-1;
    
    //create picker view for clients dropdown
    _clients = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 200, 320, 200)];
    _clients.delegate = self;
    _clients.showsSelectionIndicator = YES;
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    UIBarButtonItem *doneButtonForClients = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:nil action:@selector(doneEditingClients)];
    UIBarButtonItem *flexibleSpaceMiddleForClients = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *closeButtonForClients = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:nil action:@selector(closeEditingClients)];
    [pickerToolbar setItems:[NSArray arrayWithObjects:doneButtonForClients, flexibleSpaceMiddleForClients, closeButtonForClients, nil]];
    
    _clientsInput.inputView = _clients;
    _clientsInput.inputAccessoryView = pickerToolbar;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Helpers
- (void)setClientsData: (NSDictionary*)clientsData
{
    customers = [[NSArray alloc] initWithArray:[[clientsData objectForKey:@"hits"] objectForKey:@"customers"]];
    [self getDataForClient:0 forClientDateFrom:@"" forClientDateTo:@"" forClientCallerID:@"" forClientTrackingNumber:@"" forClientLimitFrom:0 forClientLimitTo:25];
    _clientsInput.text = [@"" stringByAppendingFormat:@"%@", [[customers objectAtIndex:0] objectForKey:@"name"]];
    
    [[NSUserDefaults standardUserDefaults] setObject:[[clientsData objectForKey:@"hits"] objectForKey:@"jwt"] forKey:kLoginSession];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)getDataForClient: (NSInteger)clientRow forClientDateFrom:(NSString*)clientDateFrom forClientDateTo:(NSString*)clientDateTo forClientCallerID:(NSString*)clientCallerID forClientTrackingNumber:(NSString*)clientTrackingNumber forClientLimitFrom:(NSInteger)clientLimitFrom forClientLimitTo:(NSInteger)clientLimitTo
{
    serverReturnList = @"";
    
    // Create your request string with parameter name as defined in PHP file
    NSString *requestString = [NSString stringWithFormat:@"customer_id=%@&date_from=%@&date_to=%@&caller_id=%@&tracking_number=%@&select_from=%ld&select_limit=%ld", [[customers objectAtIndex:clientRow] objectForKey:@"customer_id"], clientDateFrom, clientDateTo, clientCallerID, clientTrackingNumber, (long)clientLimitFrom, (long)clientLimitTo];
    
    // Create Data from request
    NSData *requestData = [NSData dataWithBytes: [requestString UTF8String] length: [requestString length]];
//    NSURL *authURL = [NSURL URLWithString:@"http://calltrack_test.ms.vmtinternal.net/api/recordings"];
    NSURL *authURL = [NSURL URLWithString:@"http://calltrack.vmtinternal.net/api/recordings"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: authURL];
    
    // set Request Type
    [request setHTTPMethod: @"POST"];
    // Set content-type
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    // Set Request Body
    [request setHTTPBody: requestData];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)doneEditingDate
{
    if ([_dateFrom isFirstResponder]) {
        UIDatePicker *picker = (UIDatePicker*)_dateFrom.inputView;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSString *dateString = [dateFormat stringFromDate:picker.date];
        
        _dateFrom.text = [NSString stringWithFormat:@"%@",dateString];
        
        [_dateFrom resignFirstResponder];
    }
    if ([_dateTo isFirstResponder]) {
        UIDatePicker *picker = (UIDatePicker*)_dateTo.inputView;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSString *dateString = [dateFormat stringFromDate:picker.date];
        
        _dateTo.text = [NSString stringWithFormat:@"%@",dateString];
        
        [_dateTo resignFirstResponder];
    }
}

- (void)closeEditingDate
{
    if ([_dateFrom isFirstResponder]) {
        [_dateFrom resignFirstResponder];
    }
    if ([_dateTo isFirstResponder]) {
        [_dateTo resignFirstResponder];
    }
}

- (void)doneEditingClients
{
    UIPickerView *clientsPicker = (UIPickerView*)_clientsInput.inputView;
    _clientsInput.text = [@""stringByAppendingFormat:@"%@", [[customers objectAtIndex:[clientsPicker selectedRowInComponent:0]] objectForKey:@"name"]];
        
    [_clientsInput resignFirstResponder];
    
    currentResultNo = 0;
    
    [self getDataForClient:[clientsPicker selectedRowInComponent:0] forClientDateFrom:_dateFrom.text forClientDateTo:_dateTo.text forClientCallerID:_callerIDFilter.text forClientTrackingNumber:_trackingNumberFilter.text forClientLimitFrom:0 forClientLimitTo:25];
}

- (void)closeEditingClients
{
    [_clientsInput resignFirstResponder];
}

#pragma mark - IBActions
- (IBAction)searchKeyWord:(id)sender {
    //[HUD show:YES];
    
    UIPickerView *clientsPicker = (UIPickerView*)_clientsInput.inputView;
    
    currentResultNo = 0;
    
    [self getDataForClient:[clientsPicker selectedRowInComponent:0] forClientDateFrom:_dateFrom.text forClientDateTo:_dateTo.text forClientCallerID:_callerIDFilter.text forClientTrackingNumber:_trackingNumberFilter.text forClientLimitFrom:0 forClientLimitTo:25];
}

- (IBAction)clientsDropdownAction:(id)sender {
    [_clientsInput becomeFirstResponder];
}

#pragma mark - Picker view delegates
- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {

}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSUInteger numRows = customers.count;
    
    return numRows;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *title;
    title = [@"" stringByAppendingFormat:@"%@",[[customers objectAtIndex:row] objectForKey:@"name"]];
    
    return title;
}

#pragma mark - Network Connection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	expectedLength = MAX([response expectedContentLength], 1);
	currentLength = 0;
	HUD.mode = MBProgressHUDModeDeterminate;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	currentLength += [data length];
	HUD.progress = currentLength / (float)expectedLength;
    
    serverReturnList = [serverReturnList stringByAppendingString:[[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding]];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSData *JSONdata = [serverReturnList dataUsingEncoding:NSUTF8StringEncoding];
    
    serverParsedResponse = [NSJSONSerialization JSONObjectWithData:JSONdata options:0 error:0];
	
    NSString *responseSuccess = [serverParsedResponse objectForKey:@"success"];
    NSString *responseError = [serverParsedResponse objectForKey:@"errors"];
    
    int successLevel = [responseSuccess intValue];
    
    if (successLevel == 1) {
        fullRecordList = [serverParsedResponse objectForKey:@"hits"];
        totalHits = [serverParsedResponse objectForKey:@"total_hits"];
        
        if ( currentResultNo == 0 ) {
            currentResultNo = fullRecordList.count;
            if (currentResultNo < 25) {
                currentResultNo = 25;
            }
            
            recordsList = fullRecordList;
        } else {
            currentResultNo = currentResultNo+25;
            
            NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
            
            for (int i=0; i<[fullRecordList count]; i++) {
                [tmpArray addObject:[fullRecordList objectAtIndex:i]];
            }
            recordsList = [recordsList arrayByAddingObjectsFromArray:fullRecordList];
        }
        
        //[self searchKeyWord:nil];
    } else if (successLevel == 2) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Error"
                                                          message:responseError
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        
        [message show];
    }
	HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
	HUD.mode = MBProgressHUDModeCustomView;
	[HUD hide:YES];
    [_tb reloadData];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[HUD hide:YES];
}

#pragma mark - table delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return recordsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"CustomCell";
    ListTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = (ListTableCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *recordsInfo = [NSDictionary dictionaryWithDictionary:[recordsList objectAtIndex:indexPath.row]];
    
    [cell setCellData:recordsInfo forCellId:indexPath.row];
    [cell setParent:self];
    //check if it has a playing row
    if (_currentPlaying>=0){
        //on creating the cell, we will look if the current row has a play status
        if (_currentPlaying == indexPath.row){
            [cell setPlayButtonToYes];
        }
    }
    if (indexPath.row == currentResultNo - 1) {
        UIPickerView *clientsPicker = (UIPickerView*)_clientsInput.inputView;
        
        [self getDataForClient:[clientsPicker selectedRowInComponent:0] forClientDateFrom:_dateFrom.text forClientDateTo:_dateTo.text forClientCallerID:_callerIDFilter.text forClientTrackingNumber:_trackingNumberFilter.text forClientLimitFrom:currentResultNo forClientLimitTo:25];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [HUD show:YES];
    recordID = [[recordsList objectAtIndex:indexPath.row] objectForKey:@"uuid"];
    
    NSString *start_stamp = [[recordsList objectAtIndex:indexPath.row] objectForKey:@"start_stamp"];
    NSString *rc = [[recordsList objectAtIndex:indexPath.row] objectForKey:@"rc"];
    NSString *caller_id_number = [[recordsList objectAtIndex:indexPath.row] objectForKey:@"caller_id_number"];
    NSString *destination_number = [[recordsList objectAtIndex:indexPath.row] objectForKey:@"destination_number"];
    NSString *fwd_number = [[recordsList objectAtIndex:indexPath.row] objectForKey:@"fwd_number"];
    NSString *name = [[recordsList objectAtIndex:indexPath.row] objectForKey:@"name"];
    NSString *duration = [[recordsList objectAtIndex:indexPath.row] objectForKey:@"duration"];
    NSString *is_valid_lead = [[recordsList objectAtIndex:indexPath.row] objectForKey:@"is_valid_lead"];
    
    NSString *uiMessage = [[NSString alloc] initWithString:[NSString stringWithFormat:@"Date/Time: %@\nCaller Location: %@\nCaller Number: %@\nTracking Number: %@\nTarget Number: %@\nBuilding Address: %@\nDuration (s): %@\nStatus: %@", start_stamp, rc, caller_id_number, destination_number, fwd_number, name, duration, is_valid_lead]];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Record"
                                              message:uiMessage
                                              delegate:self
                                              cancelButtonTitle:@"Close"
                                              otherButtonTitles:nil];
    [alert show];
    [HUD hide:YES];
}

- (void)starPlayForRow: (NSInteger)playingRec {
    if(_currentPlaying >=0)
    {
        if (playingRec>=0){
            //will get the cell who is playing, to change his status
            [(ListTableCell*)[_tb cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_currentPlaying inSection:0]] stopPlaying];
        }
    }
    _currentPlaying = playingRec;
}
@end
