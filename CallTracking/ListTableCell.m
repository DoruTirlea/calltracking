//
//  ListTableCell.m
//  CallTracking
//
//  Created by Tirlea Doru on 7/27/14.
//  Copyright (c) 2014 VoiceMailTel.Inc. All rights reserved.
//

#import "ListTableCell.h"
#import <AVFoundation/AVFoundation.h>

@implementation ListTableCell

@synthesize startStamp;
@synthesize rc;
@synthesize callerIdNumber;
@synthesize destinationNumber;
@synthesize name;
@synthesize duration;
@synthesize rec;
@synthesize parent;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setCellData:(NSDictionary *)cellInfo forCellId:(NSInteger)cellId
{
    [startStamp setText:[cellInfo objectForKey:@"start_stamp"]];
    [rc setText: [cellInfo objectForKey:@"rc"]];
    [callerIdNumber setText: [cellInfo objectForKey:@"caller_id_number"]];
    [destinationNumber setText: [NSString stringWithFormat:@"%@\n%@", [cellInfo objectForKey:@"destination_number"], [cellInfo objectForKey:@"fwd_number"]]];
    [name setText:[cellInfo objectForKey:@"name"]];
    [duration setText:[cellInfo objectForKey:@"duration"]];
    recordID = [cellInfo objectForKey:@"uuid"];
    [rec setSelected:NO];
    _playerStatus = NO;
    _cellId = cellId;
}

- (IBAction)playRec:(id)sender {
    NSString *loginSession = [[NSUserDefaults standardUserDefaults] objectForKey:kLoginSession];
    
//    NSString *urlString = [@"" stringByAppendingFormat:@"http://calltrack_test.ms.vmtinternal.net/recordings/streaming/%@?h=%@", recordID, loginSession];
    NSString *urlString = [@"" stringByAppendingFormat:@"http://calltrack.vmtinternal.net/recordings/streaming/%@?h=%@", recordID, loginSession];
    NSURL* urlStream = [NSURL URLWithString:urlString];
  
    NSError *error;
    NSError *error2;
    NSData *recFile = [[NSData alloc] initWithContentsOfURL:urlStream options:NSDataReadingMappedIfSafe error:&error ];
    audioPlayer = [[AVAudioPlayer alloc] initWithData:recFile error:&error2];

    if(_playerStatus) {
        [audioPlayer pause];
        [sender setSelected:NO];
        [parent starPlayForRow:-1];
        _playerStatus = NO;
    }
    else
    {
        [parent starPlayForRow:_cellId];
        [audioPlayer play];
        [sender setSelected:YES];
        _playerStatus = YES;
        
    }
}

-(void) stopPlaying{
    [parent starPlayForRow:-1];
    [audioPlayer pause];
    [rec setSelected:NO];
     _playerStatus = NO;
}

-(void) setPlayButtonToYes{
    _playerStatus = YES;
    [rec setSelected:YES];
}
@end
