//
//  ViewController.m
//  CallTracking
//
//  Created by Tirlea Doru on 7/25/14.
//  Copyright (c) 2014 VoiceMailTel.Inc. All rights reserved.
//

#import "ViewController.h"
#import "ListViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)rememberMe:(id)sender {
    if (!rememberChecked) {
        [_rememberMeButton setImage:[UIImage imageNamed:@"checkBoxMarked.png"] forState:UIControlStateNormal];
        rememberChecked = YES;
    }
    
    else if (rememberChecked) {
        [_rememberMeButton setImage:[UIImage imageNamed:@"checkBox.png"] forState:UIControlStateNormal];
        rememberChecked = NO;
    }
}

- (IBAction)login:(id)sender {
    if (rememberChecked) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:_username.text  forKey:@"username"];
        [defaults setObject:_password.text  forKey:@"password"];
        [defaults synchronize];
        //_username.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    }
    
    serverReturnList = @"";
    
    // Create your request string with parameter name as defined in PHP file
    NSString *requestString = [NSString stringWithFormat:@"username=%@&password=%@", _username.text, _password.text];
    
    // Create Data from request
    NSData *requestData = [NSData dataWithBytes: [requestString UTF8String] length: [requestString length]];
//    NSURL *authURL = [NSURL URLWithString:@"http://calltrack_test.ms.vmtinternal.net/api/auth"];
    NSURL *authURL = [NSURL URLWithString:@"http://calltrack.vmtinternal.net/api/auth"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: authURL];
    
    // set Request Type
    [request setHTTPMethod: @"POST"];
    // Set content-type
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    // Set Request Body
    [request setHTTPBody: requestData];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [_username resignFirstResponder];
    [_password resignFirstResponder];
}

#pragma mark - Network Connection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	expectedLength = MAX([response expectedContentLength], 1);
	currentLength = 0;
	HUD.mode = MBProgressHUDModeDeterminate;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	currentLength += [data length];
	HUD.progress = currentLength / (float)expectedLength;
    
    serverReturnList = [serverReturnList stringByAppendingString:[[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding]];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    NSData *JSONdata = [serverReturnList dataUsingEncoding:NSUTF8StringEncoding];
    
    serverParsedResponse = [NSJSONSerialization JSONObjectWithData:JSONdata options:0 error:0];

    NSString *responseSuccess = [serverParsedResponse objectForKey:@"success"];
    NSString *responseError = [serverParsedResponse objectForKey:@"errors"];
    
    int successLevel = [responseSuccess intValue];
    
    if (successLevel == 1) {
        [self performSegueWithIdentifier:@"listViewAction" sender:nil];
    } else {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Error"
                                                          message:responseError
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        
        [message show];
    }
    
	HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
	HUD.mode = MBProgressHUDModeCustomView;
	[HUD hide:YES afterDelay:1];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[HUD hide:YES];
}

#pragma mark - Seque delegates
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"listViewAction"])
    {
        // Get reference to the destination view controller
        ListViewController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        [vc setClientsData:serverParsedResponse];
    }
}
@end
