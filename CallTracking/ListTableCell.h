//
//  ListTableCell.h
//  CallTracking
//
//  Created by Tirlea Doru on 7/27/14.
//  Copyright (c) 2014 VoiceMailTel.Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "ListViewController.h"

@interface ListTableCell : UITableViewCell {
    UILabel *_startStamp;
    UILabel *_rc;
    UILabel *_callerIdNumber;
    UILabel *_destinationNumber;
    UILabel *_name;
    UILabel *_duration;
    UIButton *_rec;
    NSString *recordID;
    AVAudioPlayer *audioPlayer;
    ListViewController *_parent;
    NSInteger _cellId;
}

@property (assign, nonatomic) IBOutlet UILabel *startStamp;
@property (assign, nonatomic) IBOutlet UILabel *rc;
@property (assign, nonatomic) IBOutlet UILabel *callerIdNumber;
@property (assign, nonatomic) IBOutlet UILabel *destinationNumber;
@property (assign, nonatomic) IBOutlet UILabel *name;
@property (assign, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet UIButton *rec;

@property (strong, nonatomic) ListViewController *parent;
@property (assign, nonatomic) BOOL playerStatus;

- (void)setCellData: (NSDictionary*)cellInfo forCellId:(NSInteger) cellId;
- (IBAction)playRec:(id)sender;
- (void)stopPlaying;
- (void) setPlayButtonToYes;

@end
