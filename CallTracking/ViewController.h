//
//  ViewController.h
//  CallTracking
//
//  Created by Tirlea Doru on 7/25/14.
//  Copyright (c) 2014 VoiceMailTel.Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface ViewController : UIViewController{
    MBProgressHUD *HUD;
    NSDictionary *serverParsedResponse;
    NSString *serverReturnList;
    
    BOOL rememberChecked;
    long long expectedLength;
	long long currentLength;
}


@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *rememberMeButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

- (IBAction)rememberMe:(id)sender;
- (IBAction)login:(id)sender;


@end
