//
//  ListViewController.h
//  CallTracking
//
//  Created by Tirlea Doru on 7/27/14.
//  Copyright (c) 2014 VoiceMailTel.Inc. All rights reserved.
//

#import "ViewController.h"

#define kLoginSession @"LoginSession"

@interface ListViewController : ViewController <UIAlertViewDelegate, UIPickerViewDelegate> {
    NSArray *customers;
    NSArray *recordsList;
    NSArray *fullRecordList;
    NSString *totalHits;
    NSInteger currentResultNo;
    NSString *recordID;
    NSString *radioURL;
    //because cells objects are reused, we will keep playing status based on the index, and not on the object
    NSInteger _currentPlaying;
}

@property (weak, nonatomic) IBOutlet UITextField *clientsInput;
@property (weak, nonatomic) IBOutlet UIButton *clientsButton;
@property (weak, nonatomic) IBOutlet UITableView *tb;
@property (weak, nonatomic) IBOutlet UITextField *dateFrom;
@property (weak, nonatomic) IBOutlet UITextField *dateTo;
@property (weak, nonatomic) IBOutlet UITextField *callerIDFilter;
@property (weak, nonatomic) IBOutlet UITextField *trackingNumberFilter;


@property (strong,nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet UIPickerView *clients;
@property (strong, nonatomic) UITableView* tableView;
@property (retain, nonatomic) UIToolbar *dateToolbar;

- (void)setClientsData: (NSDictionary*)clientsData;
- (void)getDataForClient: (NSInteger)clientRow forClientDateFrom:(NSString*)clientDateFrom forClientDateTo:(NSString*)clientDateTo forClientCallerID:(NSString*)clientCallerID forClientTrackingNumber:(NSString*)clientTrackingNumber forClientLimitFrom:(NSInteger)clientLimitFrom forClientLimitTo:(NSInteger)clientLimitTo;
//because cells objects are reused, we will keep playing status based on the index, and not on the object
- (void)starPlayForRow: (NSInteger)playingRec;

- (IBAction)searchKeyWord:(id)sender;
- (IBAction)clientsDropdownAction:(id)sender;


@end
