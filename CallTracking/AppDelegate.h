//
//  AppDelegate.h
//  CallTracking
//
//  Created by Tirlea Doru on 7/25/14.
//  Copyright (c) 2014 VoiceMailTel.Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
